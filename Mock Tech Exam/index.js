function countLetter(letter, sentence) {
    let count = 0;

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.
 
    if (typeof letter === 'string' && letter.length === 1) {
        for (let i = 0; i < sentence.length; i++) {
            if (sentence[i] === letter) {
                count++;
            }
        }
        return count;
    } else {
        
        return undefined;
    }
}

const sentence = "You can do this, Jonah!";
const letterToCount = 'o';

const result = countLetter(letterToCount, sentence);
console.log(`The letter "${letterToCount}" appears ${result} times in the sentence.`);


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
    
    text = text.toLowerCase();

    const letterSeen = {};

    for (let i = 0; i < text.length; i++) {
        const letter = text[i];

 
        if (letterSeen[letter]) {
            return false;
        }

    
        letterSeen[letter] = true;
    }

    return true;
}

console.log(isIsogram("inconvenient")); // false
console.log(isIsogram("formula")); // true 
console.log(isIsogram("JavaScript")); // false 


function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    
   if (age < 13) {
        return undefined;
    } else if (age >= 13 && age <= 21) {
        
        const discountedPrice = (price * 0.8).toFixed(2);
        return discountedPrice;
    } else if (age >= 65) {
        
        const discountedPrice = (price * 0.8).toFixed(2);
        return discountedPrice;
    } else if (age >= 22 && age <= 64) {
        
        const roundedPrice = price.toFixed(2);
        return roundedPrice;
    } else {
        
        return price.toFixed(2);
    }
}

console.log(purchase(10, 100)); // undefined
console.log(purchase(18, 50)); // 40 (20% discount applied)
console.log(purchase(30, 75.5)); // 76 (rounded off)
console.log(purchase(70, 120)); // 120 (no discount applied)


function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

    const hotCategories = [];

    for (const item of items) {
        if (item.stocks === 0) {
           
            hotCategories.push(item.category);
        }
    }

 
    const uniqueHotCategories = [...new Set(hotCategories)];

    return uniqueHotCategories;
}

// Test data
const items = [
    { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
    { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
    { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
    { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
    { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
];

const hotCategories = findHotCategories(items);
console.log(hotCategories); // Output: ['toiletries', 'gadgets']



function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.

    const commonVoters = [];

    
    for (const voter of candidateA) {
        
        if (candidateB.includes(voter)) {
            
            commonVoters.push(voter);
        }
    }

    return commonVoters;
}

// Test data
const candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'];
const candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l'];


const commonVoters = findFlyingVoters(candidateA, candidateB);
console.log(commonVoters); // Output: ['LIWf1l', 'V2hjZH']


module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};


